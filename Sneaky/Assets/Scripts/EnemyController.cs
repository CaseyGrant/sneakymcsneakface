﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float enemySpeed;
    private Rigidbody2D rb;
    private Transform tf;
    private Vector3 direction;
    public PawnControl pawn;

    [SerializeField] private FieldOfView FieldOfView;
    [SerializeField] private HearingArea HearingArea;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        tf = GetComponent<Transform>();
        FieldOfView = Instantiate(FieldOfView);
        HearingArea =  Instantiate(HearingArea);
    }

    // Update is called once per frame
    void Update()
    {
        //if ()
        //{

        //}
        pawn.MoveForward(enemySpeed, tf, rb);
        direction = (tf.position - GameManager.instance.playerPosition.position).normalized;
        tf.up = -direction;

        FieldOfView.SetAimDirection(direction);
        FieldOfView.SetOrigin(tf.position);
        HearingArea.SetOrigin(tf.position);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(other.gameObject);
        }
    }
}
