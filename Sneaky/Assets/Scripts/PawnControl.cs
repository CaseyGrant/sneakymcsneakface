﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnControl : MonoBehaviour
{
    public int weapon = 3;
    public float pistolFireRate;
    public float rifleFireRate;
    public float shotgunFireRate;
    private float fireRate;
    public float nextFire;
    public GameObject shot;
    public Sprite pistol;
    public Sprite rifle;
    public Sprite shotgun;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot(Transform tf)
    {
        if (weapon == 1)
        {
            fireRate = pistolFireRate;
        }
        if (weapon == 2)
        {
            fireRate = rifleFireRate;
        }
        if (weapon == 3)
        {
            fireRate = shotgunFireRate;
        }

        nextFire = Time.time + fireRate;
        Instantiate(shot, tf.position + (tf.up * 1.5f) + (tf.right * 0.48f), tf.rotation);
        if (weapon == 3)
        {
            Instantiate(shot, tf.position + (tf.up * 1.5f) + (tf.right * 0.48f), tf.rotation * Quaternion.Euler(0f, 0f, 6f));
            Instantiate(shot, tf.position + (tf.up * 1.5f) + (tf.right * 0.48f), tf.rotation * Quaternion.Euler(0f, 0f, -6f));
        }
    }

    public void MoveForward(float speed, Transform tf, Rigidbody2D rb)
    {
        rb.velocity = tf.up * speed;
    }
    public void MoveBackward(float speed, Transform tf, Rigidbody2D rb)
    {
        rb.velocity = -tf.up * speed;
    }
    public void TurnLeft(float rotationSpeed, Transform tf)
    {
        tf.Rotate(0.0f, 0.0f, rotationSpeed);
    }
    public void TurnRight(float rotationSpeed, Transform tf)
    {
        tf.Rotate(0.0f, 0.0f, -rotationSpeed);
    }
}
